var lat = null;
var lon = null;

window.addEventListener('load', function(){
  if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(loc){
        lat = loc.coords.latitude;
        lon = loc.coords.longitude;
      });
  }
});
