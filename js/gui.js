window.addEventListener('load', function(){
  [].forEach.call(document.querySelectorAll('dialog'), function(el) {dialogPolyfill.registerDialog(el)});

  for(var i = 0; i < document.querySelector('nav').childNodes.length; i++){
    [].forEach.call(document.querySelectorAll(`nav a:nth-child(${i + 1})`), function(el) {
      el.addEventListener('click', function(ev){
        ev.preventDefault();
        document.getElementById(el.getAttribute('data-open')).showModal();
      });
    });
  }

  [].forEach.call(document.querySelectorAll(`dialog .close`), function(el) {
    el.addEventListener('click', function(ev){
      ev.preventDefault();
      el.parentElement.parentElement.close();
    });
  });

  document.querySelector('#search button').addEventListener('click', function(ev){
    ev.preventDefault();

    var name = document.getElementById('searchfor').value;

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        if(xhttp.responseText == 'undefined'){
          document.getElementById('search').close();
          document.querySelector('.mdl-snackbar').MaterialSnackbar.showSnackbar({message:'Not Found on System!'});
        } else {
          var obj = JSON.parse(xhttp.responseText)
          document.getElementById('search').close();
          document.querySelector('.mdl-snackbar').MaterialSnackbar.showSnackbar({message: obj.name + '(' + obj.latitude + ', ' + obj.longitude + ')'});
        }
      } else if(this.readyState == 4){
        document.querySelector('.mdl-snackbar').MaterialSnackbar.showSnackbar({message:'Error: try again!'});
      }
    }

    xhttp.open('GET', 'api/' + name, true);
    xhttp.send();

  });

  document.querySelector('#register button').addEventListener('click', function(ev){
    ev.preventDefault();

    var name = document.getElementById('name').value;
    var mail = document.getElementById('mail').value;
    var phone = document.getElementById('phone').value;

    var nreg = /^([A-Z][^0-9!@#\$%\^\&*\)\(+=._-]*\s?)*$/;
    var nmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var nphone = /^([0-9]{2})?(\([0-9]{2})\)([8-9][0-9]{4}|[0-9]{4})-[0-9]{4}$/;

    if(name.match(nreg) && mail.match(nmail) && phone.match(nphone)){

      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById('name').value = '';
          document.getElementById('mail').value = '';
          document.getElementById('phone').value = '';

          document.getElementById('register').close();
          document.querySelector('.mdl-snackbar').MaterialSnackbar.showSnackbar({message:'Registered on System!'});
        } else if(this.readyState == 4){
          document.querySelector('.mdl-snackbar').MaterialSnackbar.showSnackbar({message:'Error: try again!'});
        }
      }

      xhttp.open('POST', 'api/' + name, true);
      xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
      xhttp.send('mail=' + mail + '&phone=' + phone + '&latitude=' + lat + '&longitude=' + lon);
    } else {
      document.querySelector('.mdl-snackbar').MaterialSnackbar.showSnackbar({message:'Error: try again!'});
    }
  });
});

