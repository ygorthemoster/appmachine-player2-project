const step = Math.PI / 100;
var n = 0.5;

var color = 0;

var angle = 0.005;
var currentN = null;
var curDur = 0;

function Animate(){
  let ctx = document.getElementById('coolAnimation').getContext('2d');
  ctx.clearRect( 0, 0, ctx.canvas.width, ctx.canvas.height);

  ctx.translate(ctx.canvas.width/2, ctx.canvas.height/2);
  ctx.rotate(angle);
  ctx.translate(-ctx.canvas.width/2, -ctx.canvas.height/2);

  for(let theta = 0; theta <= 2 * Math.PI; theta += step){
    let size = (ctx.canvas.height > ctx.canvas.width) ? (ctx.canvas.width / 3) : (ctx.canvas.height / 3)

    let cos = Math.cos(theta);
    let sin = Math.sin(theta);

    let x = Math.pow(Math.abs(cos), 2 / n) * size * (cos == 0? 0 : cos > 0? 1 : -1);
    let y = Math.pow(Math.abs(sin), 2 / n) * size * (cos == 0? 0 : sin > 0? 1 : -1);

    x += ctx.canvas.width / 2;
    y += ctx.canvas.height / 2;

    ctx.lineTo(x, y);

    if(theta == 0) ctx.beginPath();
  }

  ctx.closePath();
  ctx.fillStyle = `hsl(${color}, 78%, 30%)`;
  ctx.fill();

  color = (color + 1) % 360;

  if(curDur > 0){
    n += current;
    curDur--;
  } else {
    if(n > 1.5) current = -1;
    else if(n < 1) current = 1;
    else current = (Math.random() < 0.5) ? 1 : -1;

    let sizeDiff = Math.random();
    let delayAmount = Math.random() * 300;

    current *= sizeDiff / delayAmount;
    curDur = Math.floor(delayAmount);

    if(Math.random() < 0.5)
      angle *= -1;
  }

  window.requestAnimationFrame(Animate);
}

function AnimFill(){
  let h = window.innerHeight;
  let w = window.innerWidth;

  h -= document.querySelector('header').clientHeight;

  document.getElementById('coolAnimation').height = h;
  document.getElementById('coolAnimation').width = w;
}

window.addEventListener('load', function(){
  AnimFill();
  Animate();
});

window.addEventListener('resize', AnimFill);
