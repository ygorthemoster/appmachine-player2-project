console.log('loading libs...');
const firebase = require('firebase');
const express = require('express');
const bodyParser = require('body-parser');

console.log('initializing firebase...');

console.log('-loading configs...');
var fconf = require('./firebaseconf.json');

console.log('-starting firebase...');
const fire = firebase.initializeApp(fconf);

console.log('-starting firebase realtime database...');
var database = fire.database();

console.log('initializing express...');
const app = express();

console.log('-setting up body parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

console.log('-setting up statics');
app.use('/', express.static('../html'));
app.use('/css', express.static('../css'));
app.use('/img', express.static('../img'));
app.use('/js', express.static('../js'));

console.log('-setting up api GET...');
app.get('/api/:name', (req, res) => {
  if(req.params.name != undefined){
    database.ref(`/people/${req.params.name}`).once('value').then((snap) => {
      let obj = 'undefined';
      if(snap.val() !== null)
        obj = {
          name: snap.val().name,
          mail: snap.val().mail,
          phone: snap.val().phone,
          latitude: snap.val().lat,
          longitude: snap.val().long
        }

      res.send(obj);
    });

  } else {
    res.status(400);
    res.send('name not specified');
  }
});

console.log('-setting up api POST...');
app.post('/api/:name', (req, res) => {
  if(req.params.name != undefined){

    let obj = {
      name: req.params.name,
      mail: req.body.mail,
      phone: req.body.phone,
      lat: req.body.latitude,
      long: req.body.longitude
    }

    for(let index in obj){
      obj[index] = obj[index] || null;
    }

    database.ref(`/people/${req.params.name}`).set(obj);
    res.send('created');

  } else {
    res.status(400);
    res.send('name not specified');
  }
});

console.log('-setting up api DELETE...');
app.delete('/api/:name', (req, res) => {
  if(req.params.name != undefined){
    database.ref(`/people/${req.params.name}`).remove();
    res.send('deleted');
  } else {
    res.status(400);
    res.send('name not specified');
  }
});

console.log('starting app on port 8080...');
app.listen(8080, () => {console.log('-listening on port 8080!');});
