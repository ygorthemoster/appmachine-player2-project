# Player 2 project for Appmachine

This project is part of the selection process for appmachine's player 2, as I was
'late' for the process I'll be working on a project that can cover the most requisites
at the same time.

The project has a Node REST API that access a firebase database, and a interface
made using mdl for the API.

## Running
>    cd node

>    node server.js

## Authors

* **Ygor de Paula** - *Initial work*

